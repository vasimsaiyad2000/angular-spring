var service = angular.module('app.service', []);

service.service('apiService', function($rootScope, $http, $location, $timeout){
    
	this.get = function(apiUrl, callback){
		var headers = {};
		var userCookie = $rootScope.getCurrentUser();
		
		if(!angular.isUndefined(userCookie)){
			 headers.access_token = userCookie.authToken;
			 headers.id = userCookie.id
    	}
		
		$http.get(apiUrl, {headers : headers}).success(function(response) {
			responseHandler(response, callback);
		});
	}

	this.post = function(apiUrl, data, callback){
		
		var headers = {};
		var userCookie = $rootScope.getCurrentUser();
		
		if(!angular.isUndefined(userCookie)){
			 headers.access_token = userCookie.authToken;
			 headers.id = userCookie.id
		}
		
		$http.post(apiUrl, data, {headers : headers}).success(function(response) {
			responseHandler(response, callback);
        });
	}

	var responseHandler = function(response, callback){
		
		switch (response.status) {
			case 200:
				if(callback) callback(response);
				break;
		    case 204:
		    case 500:
		    	$rootScope.error(response.message);
			    break;
		    case 401:
		        $timeout(function(){
		        	$location.path('/signin');
		        }, 1000);
		        break;
		    default:
			    if(callback) callback(response);
			    break;
		}
	}
});