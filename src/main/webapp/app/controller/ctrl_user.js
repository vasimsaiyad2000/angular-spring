'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("UserCtrl", function($scope, $routeParams, $location,$timeout, $http, apiService){
	
		$scope.user = {};
		
		$scope.signin = function(){
			apiService.post('api/user/login', $scope.user, function(response){
				$scope.setCurrentUser(response.data);
				$scope.ok(response.message);
				
				$timeout(function(){
					$location.path('/');
				}, 2000);
		 		
			});
		 };
	 
		 $scope.signup = function(){			
			 apiService.post('api/user/signup', $scope.user, function(response){
					$scope.setCurrentUser(response.data);
			 		$location.path('/');
			 });
		 };
		 
		 $scope.clearErrors();
	});
});
 /**
 * 
 */