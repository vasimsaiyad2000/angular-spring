'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("HomeCtrl", function($scope,$routeParams,$timeout, $http, $cookieStore, apiService){
		
		$scope.user = {};
		$scope.users = [];	
		
		$scope.loadUsers = function(){
			$scope.queryAll();
		 };
		 
		 $scope.queryAll = function(){
			    apiService.get('api/user/findAll', function(response){
			    	$scope.users = response.data;
			    });
		 };
	});
});
