'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("ProfileCtrl", function($scope, $routeParams, $location,$timeout, $http, apiService){
	
		$scope.user = {};
		
		$scope.editProfile = function(){
			var currentUser = $scope.getCurrentUser();
			var apiUrl = 'api/user/edit/'+ currentUser.id;
			apiService.get(apiUrl, function(response){
				$scope.user = response.data;
			});
	 	};
	 	
	 	$scope.updateProfile = function(){
	 		var apiUrl = 'api/user/edit/'+ $scope.user.id;
	 		$scope.clearErrors();

	 		apiService.post(apiUrl, $scope.user, function(response){
	 			$scope.ok(response.message);
	 		});
	 	};
	 	
	 	$scope.clearErrors();
	});
});
 