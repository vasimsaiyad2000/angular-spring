package com.demo.utils;

import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

@Component
public class SendMail {

	@Autowired
	VelocityEngine velocityEngine;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Value("#{appProperties['mail.from.email']}")
	private String fromEmail;
	
	@Async
	public void send(String toEmail, String subject, String emailTemplate, Map	<String, Object> emailParams){
		
		MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper( message );

        try
        {
           helper.setFrom(fromEmail);
           helper.setSubject(subject);
           helper.setTo(toEmail);
           
           String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, emailTemplate, "UTF-8", emailParams);
           
           helper.setText(messageBody, true);;
           mailSender.send( message );
        }
        catch( javax.mail.MessagingException  e ){
        	e.printStackTrace();
        }
	        
	}
}
