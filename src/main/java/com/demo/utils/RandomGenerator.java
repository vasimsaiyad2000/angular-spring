package com.demo.utils;

import java.util.Random;

public class RandomGenerator {
	
	public static String createRandomString(){
		return createRandomString(8);
	}
	
	public static String createRandomString(int length){
		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		
		for(int i = 0; i < length; i++){
			sb.append(str.charAt(random.nextInt(61)));
		}
		
		return sb.toString();
	}
}
