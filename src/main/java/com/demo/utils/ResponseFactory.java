package com.demo.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.constants.Response;

/**
 * The class handle success and failure response generation
 * @author indianic
 *
 */
@Component
public class ResponseFactory {

	static Logger log = Logger.getLogger(ResponseFactory.class);
	
	@Autowired
	MessageUtils messageUtils;
	
	public Map<String, Object> buildSuccessResponse(Object object){
		return buildResponse(Response.OK, object, StringUtils.EMPTY);	
	}
			
	public Map<String, Object> buildSuccessResponse(Object object, String messageKey, Object ... messageParams){
		return buildResponse(Response.OK, object, messageUtils.getMessage(messageKey, messageParams));
	}
	
	public Map<String, Object> buildFailureResponse(String messageKey, Object ... messageParams){
		return buildResponse(Response.NO_CONTENT, null, messageUtils.getMessage(messageKey, messageParams));
	}
	
	public Map<String, Object> buildServerErrorResponse(){
		return buildResponse(Response.INTERNAL_SERVER_ERROR, null, messageUtils.getMessage("internal.server.error"));
	}
	
	public Map<String, Object> buildResponse(int status, Object object, String message){
		Map<String, Object> results = null;
		
		log.info("Creating service response....");
		
		try{
			 results = new HashMap<String, Object>();
			 results.put(Response.STATUS_KEY, status);
			 results.put(Response.MESSAGE_KEY, message);
			 
			 if(object != null){
				 results.put(Response.DATA_KEY, object);
			 }
		
			 log.info("STATUS :" + status + " || " + "MESSAGE :" + message);
			 log.info("Response created succesfully");
			 return results;
			 
		}catch(Exception ex){
			return results;
		}finally{
			results = null;
		}
	}
}
