package com.demo.utils;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class MessageUtils {

	@Autowired
	MessageSource messageResource;
	
	public String getMessage(String key, Object ... params){
		return messageResource.getMessage(key, params, Locale.getDefault());
	}
}
