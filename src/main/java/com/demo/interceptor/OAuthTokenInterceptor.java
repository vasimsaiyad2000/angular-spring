package com.demo.interceptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.demo.constants.Response;
import com.demo.utils.MessageUtils;
import com.demo.utils.ResponseFactory;
import com.google.gson.Gson;

@Component
public class OAuthTokenInterceptor extends HandlerInterceptorAdapter{

	@Autowired
	ResponseFactory responseFactory;
	
	@Autowired
	MessageUtils messageUtils;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		String accessToken = request.getHeader("access_token");
		String userId = request.getHeader("id");
		
		if(StringUtils.isEmpty(accessToken)){
			sendUnAuthorizedResponse(response);
			return false;
		}
		
		return true;
	}
	
	private void sendUnAuthorizedResponse(HttpServletResponse response) throws IOException{
		
		Map<String, Object> result = responseFactory.buildResponse(Response.UNAUTHORIZED, null, messageUtils.getMessage("invalid.token.error"));
		response.setContentType("application/json");
	  	response.setCharacterEncoding("UTF-8");
	  	response.getWriter().write(new Gson().toJson(result));
	}
}
