package com.demo.repository;


import com.demo.model.User;

public interface UserRepository extends GenericRepository<User>{
	
	public User findByEmail(String email);
	
}