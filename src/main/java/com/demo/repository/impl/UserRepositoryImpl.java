package com.demo.repository.impl;


import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.demo.model.User;
import com.demo.repository.UserRepository;

@Repository
public class UserRepositoryImpl extends GenericRepositoryImpl<User> implements UserRepository {

	public User findByEmail(String email) {
		return findOne(new Query(Criteria.where("email").is(email)));
	}
}
