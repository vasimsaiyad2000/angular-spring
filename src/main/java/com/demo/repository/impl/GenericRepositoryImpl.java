package com.demo.repository.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import com.demo.repository.GenericRepository;


public abstract class GenericRepositoryImpl<T> implements GenericRepository<T> {

	@Autowired
	protected MongoTemplate mongoTemplate;
	protected Class<T> clazz;

	@SuppressWarnings("unchecked")
	protected GenericRepositoryImpl(){
		this.clazz = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public T save(T entity) {
		
		try{
			mongoTemplate.save(entity);
		}catch(Exception e){
			entity = null;
		}
		
		return entity;
	}

	public void delete(T entity) {
		mongoTemplate.remove(entity);
	}

	public void delete(Query query) {
		mongoTemplate.remove(query, this.clazz);		
	}

	public T findById(String id) {
		return mongoTemplate.findById(id, this.clazz);
	}

	public T findOne(Query query) {
		return mongoTemplate.findOne(query, this.clazz);
	}

	public List<T> findAll() {
		return mongoTemplate.findAll(this.clazz);
	}

	public List<T> find(Query query) {
		return mongoTemplate.find(query, this.clazz);
	}

	public Long count(Query query) {
		return mongoTemplate.count(query,this.clazz);
	}
}
