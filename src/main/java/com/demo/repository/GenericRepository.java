package com.demo.repository;

import java.util.List;
import org.springframework.data.mongodb.core.query.Query;

public interface GenericRepository<T> {
	
	public T save(T entity);
	public void delete(T entity);
	public void delete(Query query);
	public T findById(String id);
	public T findOne(Query query);
	public List<T> findAll();
	public List<T> find(Query query);
	public Long count(Query query);

}
