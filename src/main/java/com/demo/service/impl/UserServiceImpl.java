package com.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.User;
import com.demo.repository.UserRepository;
import com.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	public User registerUser(User user) {
		return userRepository.save(user);
	}

	public User authenticateUser(String email, String password) {
		return userRepository.findByEmail(email);
	}

	public List<User> listUsers() {
		return userRepository.findAll();
	}

	public boolean isEmailExist(String email) {
		return userRepository.findByEmail(email) != null ? true : false;
	}

	
	public User editProfile(String id) {
		return userRepository.findById(id);
	}

	public User updateProfile(User user) {
		User u = userRepository.findById(user.getId());
		u.setFirstName(user.getFirstName());
		u.setLastName(user.getLastName());
		
		return userRepository.save(u);
	}

}
