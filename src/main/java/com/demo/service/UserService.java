package com.demo.service;

import java.util.List;

import com.demo.model.User;

public interface UserService {

	public User registerUser(User user);
	public User authenticateUser(String email, String password);
	public List<User> listUsers();
	public boolean isEmailExist(String email);
	public User editProfile(String id);
	public User updateProfile(User user);
}
