package com.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.demo.model.User;
import com.demo.service.UserService;
import com.demo.utils.ResponseFactory;
import com.demo.utils.SendMail;

@Controller
@RequestMapping(value="/api/user")
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	ResponseFactory responseFactory;
	
	@Autowired
	SendMail sendMail;
	
	@RequestMapping(value ="/login", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> login(@RequestBody User user){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try{
			
			 User loggedInUser = userService.authenticateUser(user.getEmail(), user.getPassword());
			 
			 if(loggedInUser != null){
				 
				 if(loggedInUser.getPassword().equals(user.getPassword())){
					 loggedInUser.setAuthToken(UUID.randomUUID().toString());
					 resultMap = responseFactory.buildSuccessResponse(loggedInUser, "user.login.success");
				 }else{
					 resultMap = responseFactory.buildFailureResponse("incorrect.password.error");
				 }
				
			 }else{
				 resultMap = responseFactory.buildFailureResponse("incorrect.email.error");
			 }
		}catch(Exception e){	
			resultMap =  responseFactory.buildServerErrorResponse();
		}
		
		return resultMap;
	}
	
	@RequestMapping(value ="/signup", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> register(@RequestBody User user){
		Map<String, Object> resultMap = new HashMap<String, Object>();
				
		try{
			
			if(!userService.isEmailExist(user.getEmail())){
				
				User registeredUser = userService.registerUser(user);
				
				if(registeredUser != null){
					 /*sendMail.send(registeredUser.getEmail(), "You have registered Successfully", 
							 	"signupConfirm.vm", userMap);*/
					 resultMap = responseFactory.buildSuccessResponse(registeredUser);
				 }
			}else{
				resultMap = responseFactory.buildFailureResponse("email.exist.error");
			}
			
		  return resultMap;

		}catch(Exception e){	
			return responseFactory.buildServerErrorResponse();
		}finally{
			resultMap = null;
		}
	}
	
	@RequestMapping(value ="/findAll", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> findAll(){
		
		try{
			 List<User> users = userService.listUsers();
			 return responseFactory.buildSuccessResponse(users);

		}catch(Exception e){	
			return responseFactory.buildServerErrorResponse();
		}
	}
	
	@RequestMapping(value ="/edit/{id}", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> editProfileGet(@PathVariable("id") String id){
		
		try{
			 
			 User user = userService.editProfile(id);
			 return responseFactory.buildSuccessResponse(user);

		}catch(Exception e){	
			return responseFactory.buildServerErrorResponse();
		}
	}
	
	@RequestMapping(value ="/edit/{id}", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> editProfilePost(@PathVariable("id") String id, @RequestBody User userCommand){
		
		try{
			 User  user = userService.updateProfile(userCommand);
			 return responseFactory.buildSuccessResponse(user, "profile.update.success");

		}catch(Exception e){	
			return responseFactory.buildServerErrorResponse();
		}
	}
}
