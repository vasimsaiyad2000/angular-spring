package com.demo.constants;

public class Response {

	public static final String STATUS_KEY = "status";
	public static final String MESSAGE_KEY = "message";
	public static final String DATA_KEY = "data";
	public static final int OK = 200;
	public static final int NO_CONTENT = 204;
	public static final int INTERNAL_SERVER_ERROR = 500;
	public static final int UNAUTHORIZED = 401;
	
}
