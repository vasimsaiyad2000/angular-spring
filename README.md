# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Pre-requisites ###

* JDK1.7
* MAVEN
* MONGODB


### How do I get set up mongodb? ###

Change the mongodb configuration in **application.properties** file under **src/main/resources** folder.

**NOTE :** Create a blank database in mongodb named **angulardemo** to perform the database operations.

### How do I run the project ? ###

Go to project directory from command line and run the below command.

**mvn clean install tomcat7:run**

Now, Open http:localhost:8080/ in browser window.
